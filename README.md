# README #

From Wikipedia:
In machine learning, one-class classification, also known as unary classification, tries to identify objects of a specific class amongst all objects, by learning from a training set containing only the objects of that class. This is different from and more difficult than the traditional classification problem, which tries to distinguish between two or more classes with the training set containing objects from all the classes.

### What is this repository for? ###

* Quick summary: one-class classification of fic dataset using PIG

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions


### Who do I talk to? ###

* Mathieu Dumoulin (mathieu.dumoulin@gmail.com)