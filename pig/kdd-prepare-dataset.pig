--left for documentation purposes only. use with a .param file preferably
--%default DATA_DIR ../data/kdd1999
--%default DATA_FILE kddcup.data_10_percent.gz
--%default OUTPUT_DIR ../data/kdd1999
--%default LIB_DIR lib
--%default PARALLEL_OPTION 3
--%default POSITIVES_LIMIT 1000
--SET default_parallel $PARALLEL_OPTION;

REGISTER $LIB_DIR/caissepop*.jar;

DEFINE VECTORIZE_SERVICE com.fujitsu.ca.fic.caissepop.evaluation.kdd1999.VectorizeService();
DEFINE VECTORIZE_PROTOCOL_TYPE com.fujitsu.ca.fic.caissepop.evaluation.kdd1999.VectorizeProtocolType();
DEFINE VECTORIZE_FLAG com.fujitsu.ca.fic.caissepop.evaluation.kdd1999.VectorizeFlag();
DEFINE VECTORIZE_LABEL com.fujitsu.ca.fic.caissepop.evaluation.kdd1999.VectorizeBinaryLabel();

rmf $OUTPUT_DIR/train
rmf $OUTPUT_DIR/train-positives
rmf $OUTPUT_DIR/test
rmf $OUTPUT_DIR/unlabeled

--uses .pig_schema produced by getschema-kdd.pig
kdd_data = LOAD '$DATA_DIR/$DATA_FILE' USING PigStorage(',','-schema');

kdd_vectorized = FOREACH kdd_data GENERATE 
    VECTORIZE_LABEL(REPLACE(label, '\\.', '')) as vlabel:int, 
    duration, VECTORIZE_PROTOCOL_TYPE(protocol_type) as vprotocol_type:int, 
    VECTORIZE_SERVICE(LOWER(service)) as vservice:int, VECTORIZE_FLAG(flag) as vflag:int, 
    src_bytes..dst_host_srv_rerror_rate;

kdd_data_rnd = foreach kdd_vectorized generate *, RANDOM() as random;
kdd_data_rnd = order kdd_data_rnd by random;

SPLIT kdd_data_rnd INTO 
    unlabeled IF random < 0.9, 
    test IF (random >= 0.9 AND random <0.95), 
    train OTHERWISE;

positive_train = FILTER train BY vlabel==1;
positive_train_filtered = LIMIT positive_train $POSITIVES_LIMIT;
 
train = FOREACH train GENERATE vlabel..dst_host_srv_rerror_rate;
positive_train_filtered = FOREACH positive_train_filtered GENERATE vlabel..dst_host_srv_rerror_rate;
unlabeled = FOREACH unlabeled GENERATE vlabel..dst_host_srv_rerror_rate;
test = FOREACH test GENERATE vlabel..dst_host_srv_rerror_rate;

STORE train INTO '$OUTPUT_DIR/train' USING PigStorage(',');
STORE positive_train_filtered INTO '$OUTPUT_DIR/train-positives' USING PigStorage(',');
STORE unlabeled INTO '$OUTPUT_DIR/unlabeled' USING PigStorage(',');
STORE test INTO '$OUTPUT_DIR/test' USING PigStorage(',');
