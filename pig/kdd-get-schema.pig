REGISTER pig/lib/caissepop-1.2.jar;

DEFINE VECTORIZE_SERVICE com.fujitsu.ca.fic.caissepop.evaluation.kdd1999.VectorizeService();
DEFINE VECTORIZE_PROTOCOL_TYPE com.fujitsu.ca.fic.caissepop.evaluation.kdd1999.VectorizeProtocolType();
DEFINE VECTORIZE_FLAG com.fujitsu.ca.fic.caissepop.evaluation.kdd1999.VectorizeFlag();
DEFINE VECTORIZE_LABEL com.fujitsu.ca.fic.caissepop.evaluation.kdd1999.VectorizeBinaryLabel();

rmf data/kdd1999/out

kdd_data = LOAD 'data/kdd1999/kddcup.data_10_percent.gz' 
    USING PigStorage(',') 
    AS (duration:int, protocol_type:chararray, service:chararray, flag:chararray, 
    src_bytes:int,dst_bytes:int, land:chararray, wrong_fragment:int, 
    urgent:int, hot:int,num_failed_logins:int, logged_in:int, 
    num_compromised:int, root_shell: int, su_attempted:int, num_root:int, 
    num_file_creations: int, num_shells:int, num_access_files:int, 
    num_outbound_cmds:int, is_host_login:int, is_guest_login:int, 
    count:int, srv_count:int, serror_rate: double, srv_serror_rate: double, 
    rerror_rate: double, srv_rerror_rate: double, same_srv_rate: double, diff_srv_rate: double,
    srv_diff_host_rate: double, dst_host_count: double, dst_host_srv_count: double, 
    dst_host_same_srv_rate: double, dst_host_diff_srv_rate: double,
    dst_host_same_src_port_rate: double, dst_host_srv_diff_host_rate: double, 
    dst_host_serror_rate: double, dst_host_srv_serror_rate: double, 
    dst_host_rerror_rate: double, dst_host_srv_rerror_rate: double, label:chararray);

STORE kdd_data into 'data/kdd1999/out' USING PigStorage(',','-schema');