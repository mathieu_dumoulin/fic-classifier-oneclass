package com.fujitsu.ca.fic.mapreduce;

import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.hadoop.mrunit.mapreduce.ReduceDriver;
import org.apache.hadoop.mrunit.types.Pair;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.VectorWritable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.fujitsu.ca.fic.classifiers.perceptron.Perceptron;
import com.fujitsu.ca.fic.dataloaders.kdd1999.KddLoader;
import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;
import com.google.common.collect.Lists;

@RunWith(MockitoJUnitRunner.class)
public class FindConfidentExamplesReducerTest {
    @Mock
    private Reducer<LongWritable, VectorWritable, NullWritable, Text>.Context context;
    @Mock
    private Configuration conf;

    @Mock
    private Perceptron perceptron;

    private ReduceDriver<LongWritable, VectorWritable, NullWritable, Text> reduceDriver;

    List<VectorWritable> testVectors = Lists.newArrayList();
    List<Text> expectedText = Lists.newArrayList();
    List<String> expectedCondidenceOutputs = Lists.newArrayList("0.0", "0.1", "0.2", "0.3", "0.4");

    @Before
    public void setUp() throws Exception {
        buildInputOutput();
        when(context.getConfiguration()).thenReturn(conf);
        ReducerMock reducer = new ReducerMock(perceptron);

        reduceDriver = ReduceDriver.newReduceDriver(reducer);

        preparePerceptronMock();
    }

    @Test
    public void shouldReturnConfidenceScoreGivenOneVector() throws IOException, InterruptedException {
        ReducerMock reducer = new ReducerMock(perceptron);
        List<VectorWritable> OneExampleVectors = Lists.newArrayList();
        OneExampleVectors.add(testVectors.get(1));
        reducer.reduce(new LongWritable(1), OneExampleVectors, context);

        verify(context).write(any(NullWritable.class), any(Text.class));
    }

    @Test
    public void testThatReducerOutputsOneLineForEachInputVector() throws IOException {
        reduceDriver.withInputKey(new LongWritable(1));
        reduceDriver.withInputValues(testVectors);
        List<Pair<NullWritable, Text>> outputs = reduceDriver.run();
        assertThat(outputs.size(), is(5));
    }

    @Test
    public void shouldOutputTheExpectedConfidenceScoreForEachInputVector() throws IOException {
        reduceDriver.withInputKey(new LongWritable(1));
        reduceDriver.withInputValues(testVectors);
        List<Pair<NullWritable, Text>> outputs = reduceDriver.run();

        int i = 0;
        for (Pair<NullWritable, Text> pair : outputs) {
            String line = pair.getSecond().toString();
            String[] elements = line.split(",");
            assertThat(expectedCondidenceOutputs.get(i++), is(elements[0]));
        }
    }

    private void preparePerceptronMock() {
        for (int i = 0; i < 5; i++) {
            NamedVector example = (NamedVector) testVectors.get(i).get();
            double confidence = ((double) i) / 10;
            when(perceptron.classifyScalar(example)).thenReturn(confidence);
        }
    }

    private void buildInputOutput() throws IncorrectLineFormatException {
        KddLoader kdd = new KddLoader();
        for (int i = 0; i < 5; i++) {
            String s = buildTestFeatureString(i);
            NamedVector v = kdd.parseFields(s);

            expectedText.add(new Text(Double.toString(i) + "\t" + s));
            testVectors.add(new VectorWritable(v));
        }
    }

    private String buildTestFeatureString(double d) {
        final int FEATURES = new KddLoader().getFeaturesCount();
        String test = "1.0";
        for (int i = 0; i < FEATURES; i++) {
            test = test + "," + String.valueOf(d);
        }
        return test;
    }
}

class ReducerMock extends FindConfidentExamplesReducer {
    public ReducerMock(Perceptron perceptronMock) {
        this.perceptron = perceptronMock;
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setupTest(context);
    }
}
