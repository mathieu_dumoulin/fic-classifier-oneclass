package com.fujitsu.ca.fic.mapreduce;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;
import org.apache.hadoop.mrunit.mapreduce.MapDriver;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.VectorWritable;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.fujitsu.ca.fic.mapreduce.VectorMapper;

@RunWith(MockitoJUnitRunner.class)
public class VectorMapperTest {
    private final int NB_FIELDS = 42;
    private String testFeatures;
    double[] expectedFeatures;

    @Mock
    private Mapper<LongWritable, Text, LongWritable, VectorWritable>.Context context;

    private MapDriver<LongWritable, Text, LongWritable, VectorWritable> mapDriver;
    private static LongWritable WRITABLE_ID = new LongWritable(0);

    @Before
    public void setUp() throws Exception {
        testFeatures = buildTestFeatureString();
        expectedFeatures = buildExpectedFeatures();

        final VectorMapper mapper = new VectorMapper();
        mapper.setup(context);
        mapDriver = MapDriver.newMapDriver(mapper);
    }
    
    @Test
    public void testMap() throws IOException, InterruptedException {
        VectorWritable expected = new VectorWritable(new DenseVector(expectedFeatures));

        mapDriver.withInput(WRITABLE_ID, new Text(testFeatures));
        mapDriver.withOutput(WRITABLE_ID, expected);
        mapDriver.runTest();
    }

    private String buildTestFeatureString() {
        String test = "1.0";
        for (int i = 2; i <= NB_FIELDS; i++) {
            test = test + "," + String.valueOf(i) + ".0";
        }
        return test;
    }

    private double[] buildExpectedFeatures() {
        double[] expect = new double[NB_FIELDS];
        for (int i = 1; i <= NB_FIELDS; i++) {
            expect[i - 1] = i;
        }
        return expect;
    }
}
