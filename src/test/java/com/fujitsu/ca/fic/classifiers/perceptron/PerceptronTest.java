package com.fujitsu.ca.fic.classifiers.perceptron;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.Vector;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

import com.fujitsu.ca.fic.dataloaders.AbstractDatasetFileLoader;
import com.fujitsu.ca.fic.dataloaders.kdd1999.KddLoader;
import com.fujitsu.ca.fic.utils.Convert;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.notNullValue;

import static org.hamcrest.MatcherAssert.assertThat;

import static org.hamcrest.number.IsCloseTo.closeTo;

public class PerceptronTest {
    private AbstractDatasetFileLoader dataloader;

    private final double GAMMA = 0.55;
    private final double C = 10;

    private List<NamedVector> training;
    private List<NamedVector> postraining;
    private Vector initialResults;

    private Configuration conf;

    @Before
    public void setup() throws IOException {
        conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        conf.addResource(fs.open(new Path("conf/local-test-conf-1.xml")));
    }

    @Test
    public void checkConfigurationOk() {
        assertThat(conf.get("data.train-positives.path"), notNullValue());
        assertThat(conf.get("perceptron.model.path"), notNullValue());
    }

    @SuppressWarnings("deprecation")
    @Ignore
    public void givenKddSavedWhenLoadThenSameResults() throws IOException {
        dataloader = new KddLoader();
        String trainPosPath = conf.get("data.train-positives.path");
        postraining = dataloader.load(conf, trainPosPath);
        training = dataloader.load(conf, trainPosPath);

        List<NamedVector> positiveExamples = dataloader.load(conf, trainPosPath);
        Perceptron perceptron = Perceptron.buildTrainedPerceptron(positiveExamples, C, GAMMA);

        for (int i = 0; i < postraining.size() / 2; i++) {
            training.remove(i);
        }

        System.out.println(postraining.size() + " vs " + training.size());
        List<Boolean> labels = new ArrayList<Boolean>();
        for (NamedVector example : training) {
            labels.add(dataloader.isPositive(example));
        }

        int truePositives = 0;
        int trueNegatives = 0;
        int errorsCount = 0;
        initialResults = perceptron.classifyScalar(Convert.fromNamedVectorsToMatrix(training, dataloader.getFeaturesCount()));
        for (int i = 0; i < initialResults.size(); i++) {
            if ((labels.get(i) && initialResults.get(i) <= 0) || (!labels.get(i) && initialResults.get(i) > 0)) {
                ++errorsCount;
            } else if (labels.get(i) && initialResults.get(i) > 0) {
                ++truePositives;
            } else {
                ++trueNegatives;
            }
        }
        System.out.println("tp:" + truePositives + " tn:" + trueNegatives + "err:" + errorsCount);

        perceptron.saveToFile(conf);

        perceptron = Perceptron.loadFromFile(conf);
        Vector afterResults = perceptron.classifyScalar(Convert.fromNamedVectorsToMatrix(training, dataloader.getFeaturesCount()));

        assertThat(initialResults.size(), equalTo(afterResults.size()));

        double DELTA = 0.01;
        for (int i = 0; i < initialResults.size(); i++) {
            assertThat(initialResults.get(i), closeTo(afterResults.get(i), DELTA));
        }
    }
}
