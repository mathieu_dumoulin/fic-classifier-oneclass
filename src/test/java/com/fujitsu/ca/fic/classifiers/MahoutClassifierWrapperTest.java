package com.fujitsu.ca.fic.classifiers;

import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.*;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.mahout.classifier.sgd.AdaptiveLogisticRegression;
import org.apache.mahout.classifier.sgd.L1;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.fujitsu.ca.fic.dataloaders.kdd1999.DynamicKddLoader;
import com.google.common.collect.Lists;

public class MahoutClassifierWrapperTest {
    private static List<String> SYMBOLS = Lists.newArrayList("0", "1");
    MahoutClassifierWrapper mahoutWrapper = new MahoutClassifierWrapper(SYMBOLS);
    Configuration conf;

    @Before
    public void setup() throws IOException {
        conf = new Configuration();
        FileSystem fs = FileSystem.get(conf);
        conf.addResource(fs.open(new Path("conf/local-test-conf-1.xml")));
    }

    @After
    public void tearDown() throws Exception {
    }

    @Test
    public void givenALearnerAndAListOfExamplesWillProduceATrainedClassifier() throws Exception {
        String trainPath = conf.get("data.train.path");
        DynamicKddLoader trainDataLoader = new DynamicKddLoader(conf, trainPath);
        AdaptiveLogisticRegression adaptiveLearnerDyn = new AdaptiveLogisticRegression(trainDataLoader.getCategoriesCount(),
                trainDataLoader.getFeaturesCount(), new L1());
        mahoutWrapper.train(adaptiveLearnerDyn, trainDataLoader);

        assertThat(adaptiveLearnerDyn.auc(), is(not(Double.NaN)));
    }
}
