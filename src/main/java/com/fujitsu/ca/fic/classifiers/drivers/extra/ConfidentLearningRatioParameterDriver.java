//package com.fujitsu.ca.fic.classifiers.drivers.extra;
//
//import java.io.IOException;
//import java.util.Collections;
//import java.util.List;
//
//import org.apache.hadoop.conf.Configuration;
//import org.apache.hadoop.conf.Configured;
//import org.apache.hadoop.util.Tool;
//import org.apache.hadoop.util.ToolRunner;
//import org.apache.mahout.classifier.sgd.CrossFoldLearner;
//import org.apache.mahout.classifier.sgd.L1;
//import org.apache.mahout.math.NamedVector;
//
//import com.fujitsu.ca.fic.classifiers.MahoutClassifierWrapper;
//import com.fujitsu.ca.fic.classifiers.metrics.ClassificationMetrics;
//import com.fujitsu.ca.fic.classifiers.perceptron.Perceptron;
//import com.fujitsu.ca.fic.dataloaders.AbstractDatasetFileLoader;
//import com.fujitsu.ca.fic.dataloaders.kdd1999.KddLoader;
//import com.fujitsu.ca.fic.mapreduce.jobs.ClassifyWithPerceptronJob;
//import com.fujitsu.ca.fic.utils.Convert;
//import com.fujitsu.ca.fic.utils.pig.TopKWithPigDriver;
//import com.google.common.collect.Lists;
//
//public class ConfidentLearningRatioParameterDriver extends Configured implements Tool {
//    private final static String TRAIN_PATH = "data/kdd1999/kddtiny2";
//    private final static String TEST_PATH = "data/kdd1999/test/part-r-00000";
//    private final static String UNLABELED_PATH = "data/kdd1999/unlabeled/part-r-00000";
//
//    private static final String TOPK_OUTPUT_PATH = "data/topk/output";
//    private static final String PERCEPTRON_OUTPUT_PATH = "data/perceptron/output";
//    private static final List<Double> CONFIDENCE_TRESHOLDS = Lists.newArrayList(0.01, 0.02, 0.03, 0.04, 0.05, 0.1, 0.15, 0.2, 0.25, 0.30,
//            0.35, 0.4);
//    private static final double GAMMA = 0.55;
//    private static final double C = 1;
//    private static final int LAMBDA = 8;
//    private static final int FOLDS = 5;
//
//    private static final int MAX_RESULTS = 2000;
//
//    public static void main(String[] args) throws Exception {
//        int exitCode = ToolRunner.run(new ConfidentLearningRatioParameterDriver(), args);
//        System.exit(exitCode);
//    }
//
//    @SuppressWarnings("deprecation")
//    @Override
//    public int run(String[] arg0) throws Exception {
//        try {
//            Configuration conf = getConf();
//            AbstractDatasetFileLoader dataLoader = new KddLoader();
//            List<NamedVector> trainingExamples = dataLoader.loadPositives(conf, TRAIN_PATH);
//
//            System.out.println("Training a RBF kernel perceptron on the positives-only training set");
//            Perceptron perceptron = new Perceptron(Convert.fromNamedVectorsToMatrix(trainingExamples, dataLoader.getFeaturesCount()), GAMMA);
//            perceptron.train(C);
//
//            System.out.println();
//            System.out.println("Classifying unlabeled with trained perceptron");
//            ClassifyWithPerceptronJob classifyWithPerceptronJob = new ClassifyWithPerceptronJob(conf, UNLABELED_PATH,
//                    PERCEPTRON_OUTPUT_PATH);
//            classifyWithPerceptronJob.applyConfidenceScoreToUnlabeled(perceptron);
//
//            List<ClassificationMetrics> classificationMetrics = Lists.newArrayList();
//            List<Integer> trainSetSize = Lists.newArrayList();
//            for (Double confidence : CONFIDENCE_TRESHOLDS) {
//                System.out.println("\nGather confident examples from unlabeled");
//                TopKWithPigDriver driver = new TopKWithPigDriver(conf, PERCEPTRON_OUTPUT_PATH, TOPK_OUTPUT_PATH, dataLoader);
//                List<NamedVector> newExamplesFromUnlabeled = driver.fetchConfidentExamples(confidence, MAX_RESULTS);
//
//                System.out.println("\nRun OnlineLogisticRegression on new training set and test it");
//                List<NamedVector> newTrainingSet = Lists.newArrayList();
//                newTrainingSet.addAll(trainingExamples);
//                newTrainingSet.addAll(newExamplesFromUnlabeled);
//
//                List<String> symbols = Lists.newArrayList("0", "1");
//                CrossFoldLearner crossFoldLearner = new CrossFoldLearner(FOLDS, dataLoader.getCategoriesCount(),
//                        dataLoader.getFeaturesCount(), new L1());
//                crossFoldLearner.stepOffset(1000).decayExponent(0.9).lambda(LAMBDA).learningRate(20);
//
//                MahoutClassifierWrapper classifyWithMahout = new MahoutClassifierWrapper(symbols);
//
//                Collections.shuffle(newTrainingSet);
//                classifyWithMahout.train(crossFoldLearner, newTrainingSet);
//                classifyWithMahout.test(crossFoldLearner, dataLoader.load(conf, TEST_PATH));
//                classificationMetrics.add(classifyWithMahout.getMetrics());
//                trainSetSize.add(newTrainingSet.size());
//            }
//            int index = 0;
//            for (ClassificationMetrics metrics : classificationMetrics) {
//                System.out.println("========================================");
//                System.out.println("KeepRatio: " + CONFIDENCE_TRESHOLDS.get(index));
//                System.out.println("Train set size: " + trainSetSize.get(index));
//                metrics.showReport();
//                System.out.println();
//                index++;
//            }
//        } catch (IOException e) {
//            throw new RuntimeException(e.getMessage());
//        }
//        return 0;
//    }
// }
