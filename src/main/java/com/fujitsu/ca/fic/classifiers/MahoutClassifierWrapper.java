package com.fujitsu.ca.fic.classifiers;

import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.mahout.classifier.AbstractVectorClassifier;
import org.apache.mahout.classifier.ClassifierResult;
import org.apache.mahout.classifier.ConfusionMatrix;
import org.apache.mahout.classifier.OnlineLearner;
import org.apache.mahout.classifier.ResultAnalyzer;
import org.apache.mahout.classifier.sgd.CrossFoldLearner;
import org.apache.mahout.math.DenseVector;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.Vector;
import org.apache.mahout.math.stats.GlobalOnlineAuc;
import org.apache.mahout.math.stats.OnlineAuc;

import com.fujitsu.ca.fic.classifiers.metrics.ClassificationMetrics;
import com.fujitsu.ca.fic.dataloaders.DynamicDatasetLoader;
import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;

public class MahoutClassifierWrapper {
    private static final Log LOG = LogFactory.getLog(MahoutClassifierWrapper.class);

    private final int categories;
    private final List<String> symbols;
    private final String defaultValue = "unknown";
    private ClassificationMetrics metrics = null;

    public MahoutClassifierWrapper(List<String> symbols) {
        this.symbols = symbols;
        categories = symbols.size();
    }

    public void train(OnlineLearner onlineLearner, List<NamedVector> trainset) {
        System.out.println("Training OnlineLearner using list of vectors");
        try {
            for (NamedVector example : trainset) {
                String labelName = example.getName();
                int actual = Integer.parseInt(labelName);
                onlineLearner.train(actual, example);
            }
        } finally {
            onlineLearner.close();
        }
        System.out.println("Training complete!");
    }

    public void train(OnlineLearner onlineLearner, DynamicDatasetLoader loader) {
        System.out.println("Training OnlineLearner using dynamic dataloader");
        try {
            while (loader.hasNext()) {
                try {
                    NamedVector nextExample = loader.getNext();
                    onlineLearner.train(Integer.parseInt(nextExample.getName()), nextExample);

                } catch (IncorrectLineFormatException e) {
                    LOG.warn(e.getMessage());
                }
            }
        } finally {
            onlineLearner.close();
        }
        System.out.println("Training complete!");
    }

    public void train(OnlineLearner onlineLearner, DynamicDatasetLoader loader, int trainSetSize) {
        System.out.println("Training OnlineLearner using dynamic dataloader with max examples=" + trainSetSize);
        try {
            int nRead = 0;
            while (loader.hasNext() && nRead < trainSetSize) {
                try {
                    NamedVector nextExample = loader.getNext();
                    onlineLearner.train(Integer.parseInt(nextExample.getName()), nextExample);

                } catch (IncorrectLineFormatException e) {
                    LOG.warn(e.getMessage());
                }
                nRead++;
            }
        } finally {
            onlineLearner.close();
        }
        System.out.println("Training complete!");
    }

    public void test(AbstractVectorClassifier classifier, List<NamedVector> testset) {
        System.out.println("Training classifier with test data using list of vectors");

        ResultAnalyzer analyzer = new ResultAnalyzer(symbols, defaultValue);
        ConfusionMatrix cm = new ConfusionMatrix(symbols, defaultValue);
        OnlineAuc auc = new GlobalOnlineAuc();

        for (NamedVector example : testset) {
            String correctLabel = example.getName();
            int actual = Integer.parseInt(correctLabel);

            Vector p = new DenseVector(categories);
            classifier.classifyFull(p, example);
            int estimated = p.maxValueIndex();

            String estimatedLabel = String.valueOf(estimated);
            cm.addInstance(correctLabel, estimatedLabel);
            auc.addSample(actual, classifier.classifyScalar(example));
            analyzer.addInstance(correctLabel, new ClassifierResult(estimatedLabel));
        }

        metrics = new ClassificationMetrics(cm, auc);
        System.out.println("Testing complete!");
    }

    public void test(AbstractVectorClassifier classifier, DynamicDatasetLoader loader) {
        System.out.println("Training classifier with test data using dynamic loader");

        ResultAnalyzer analyzer = new ResultAnalyzer(symbols, defaultValue);
        ConfusionMatrix cm = new ConfusionMatrix(symbols, defaultValue);
        OnlineAuc auc = new GlobalOnlineAuc();

        while (loader.hasNext()) {
            try {
                NamedVector nextExample = loader.getNext();
                String correctLabel = nextExample.getName();
                int actual = Integer.parseInt(correctLabel);

                Vector p = new DenseVector(categories);
                classifier.classifyFull(p, nextExample);
                int estimated = p.maxValueIndex();

                String estimatedLabel = String.valueOf(estimated);
                cm.addInstance(correctLabel, estimatedLabel);

                auc.addSample(actual, classifier.classifyScalar(nextExample));
                analyzer.addInstance(correctLabel, new ClassifierResult(estimatedLabel));

                // int bump = bumps[(int) Math.floor(step) % bumps.length];
                // int scale = (int) Math.pow(10, Math.floor(step / bumps.length));
                // if (i % (bump * scale) == 0) {
                // step += 0.25;
                // System.out.printf("%5d  %10.2f     %s %s\n", i, averageCorrect * 100, correctLabel, estimatedLabel);
                // }
                // ++i;

            } catch (IncorrectLineFormatException e) {
                LOG.warn(e.getMessage());
            }
        }
        metrics = new ClassificationMetrics(cm, auc);

        System.out.println("Testing complete!");
    }

    public void showClassificationReport() {
        if (metrics == null)
            throw new RuntimeException("Must do test() before showClassificationReport()");

        metrics.showReport();
        System.out.printf("%s\n\n", metrics.confusionMatrix().toString());
    }

    public ClassificationMetrics getMetrics() {
        return metrics;
    }

    public void showClassifierParameters(CrossFoldLearner bestModelDyn) {
        System.out.println("Not implemented yet");
    }
}
