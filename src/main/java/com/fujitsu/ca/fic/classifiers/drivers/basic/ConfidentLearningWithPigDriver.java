package com.fujitsu.ca.fic.classifiers.drivers.basic;

import java.io.IOException;
import java.util.List;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.conf.Configured;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.util.Tool;
import org.apache.hadoop.util.ToolRunner;
import org.apache.mahout.classifier.sgd.CrossFoldLearner;
import org.apache.mahout.classifier.sgd.L1;
import org.apache.mahout.math.NamedVector;

import com.fujitsu.ca.fic.classifiers.MahoutClassifierWrapper;
import com.fujitsu.ca.fic.classifiers.perceptron.Perceptron;
import com.fujitsu.ca.fic.dataloaders.DatasetFileLoader;
import com.fujitsu.ca.fic.dataloaders.kdd1999.DynamicKddLoader;
import com.fujitsu.ca.fic.dataloaders.kdd1999.KddLoader;
import com.fujitsu.ca.fic.utils.pig.TopKWithPigDriver;
import com.google.common.collect.Lists;

/**
 * Drives the 2 step semi-supervised p-learning classification system job. Designed to run on a Hadoop cluster.
 * <p>
 * Flow summary:
 * <ul>
 * <li>1- Train a perceptron on the positive-only documents (of limited size 100-2000)</li>
 * <li>2- Use the trained perceptron to score the unlabeled documents of the full dataset (potentially very large) kept on the distributed
 * storage (HDFS). This is currently a MapReduce job.</li>
 * <li>3- Sort the documents by confidence, chose the top % of positives and an equivalent number of randomly selected negative examples.
 * Return the new examples. This is a Pig job.</li>
 * <li>4- Train a Mahout Online SGD Classifier on the new augmented training set. (new examples + original train set)</li>
 * <li>5- Gather metrics for the classification performance using a withheld testset.</li>
 * </ul>
 * <p>
 * The configuration is read from
 */
public class ConfidentLearningWithPigDriver extends Configured implements Tool {
    private static final String LOCAL_CONF_PATH = "conf/local-conf.xml";
    private static final String CLUSTER_CONF_PATH = "conf/cluster-conf.xml";

    private static final int LAMBDA = 8;
    private static final int FOLDS = 5;

    public static void main(String[] args) throws Exception {
        int exitCode = ToolRunner.run(new ConfidentLearningWithPigDriver(), args);
        System.exit(exitCode);
    }

    @Override
    public int run(String[] args) throws Exception {
        try {
            Configuration conf = getConf();
            if (args.length == 1 && args[0].equals("cluster")) {
                loadConfigurationFile(conf, CLUSTER_CONF_PATH);
            } else if (args.length == 1 && args[0].equals("local")) {
                loadConfigurationFile(conf, LOCAL_CONF_PATH);
            }

            DatasetFileLoader dataLoader = new KddLoader();
            String trainPositivesPath = conf.get("data.train.positives.path");
            List<NamedVector> positiveExamples = dataLoader.load(conf, trainPositivesPath);
            // List<NamedVector> positiveExamples = positiveExamples2.subList(0, 200);
            System.out.println("Training a RBF kernel perceptron on the positives-only training set");
            Perceptron perceptron = Perceptron.buildTrainedPerceptron(conf, positiveExamples);
            perceptron.saveToFile(conf);

            System.out.println("Scoring unlabeled with trained perceptron and");
            System.out.println("gathering most confident examples");
            TopKWithPigDriver driver = new TopKWithPigDriver(conf, dataLoader);
            List<NamedVector> newExamplesFromUnlabeled = driver.fetchConfidentExamples();

            System.out.println();
            System.out.println("Training Mahout classifier on augmented training set...");
            List<NamedVector> newTrainingSet = Lists.newArrayList();
            newTrainingSet.addAll(positiveExamples);
            newTrainingSet.addAll(newExamplesFromUnlabeled);

            List<String> symbols = Lists.newArrayList("0", "1");
            CrossFoldLearner crossFoldLearner = new CrossFoldLearner(FOLDS, dataLoader.getCategoriesCount(), dataLoader.getFeaturesCount(),
                    new L1());
            crossFoldLearner.stepOffset(1000).decayExponent(0.9).lambda(LAMBDA).learningRate(20);
            MahoutClassifierWrapper classifyWithMahout = new MahoutClassifierWrapper(symbols);
            classifyWithMahout.train(crossFoldLearner, newTrainingSet);
            System.out.println("Training complete!");

            System.out.println();
            System.out.println("Testing the best crossfold learner on withheld test data...");
            String testPath = conf.get("data.test.path");
            classifyWithMahout.test(crossFoldLearner, new DynamicKddLoader(conf, testPath));

            classifyWithMahout.showClassificationReport();
        } catch (IOException e) {
            throw new RuntimeException(e.getMessage());
        }
        return 0;
    }

    private void loadConfigurationFile(Configuration conf, String configFilename) throws IOException {
        FileSystem fs = FileSystem.get(conf);
        conf.addResource(fs.open(new Path(configFilename)));
    }

}
