package com.fujitsu.ca.fic.dataloaders.kdd1999;

import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.SequentialAccessSparseVector;
import org.apache.mahout.math.Vector;

import com.fujitsu.ca.fic.dataloaders.LineParser;
import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;

public class KddLineParser implements LineParser {
    private final static int FEATURES = 41;
    private final static int LABEL_INDEX = 0;

    /* (non-Javadoc)
     * @see com.fujitsu.ca.fic.dataloaders.kdd1999.LineParser#parseFields(java.lang.String)
     */
    @Override
    public NamedVector parseFields(String line) throws IncorrectLineFormatException {
        final int FIRST_FEATURE_INDEX = 1;
        double[] featuresDouble = new double[FEATURES];
        String[] features;

        try {
            features = line.split(",");

            for (int i = 0; i < FEATURES; i++) {
                featuresDouble[i] = Double.parseDouble(features[i + FIRST_FEATURE_INDEX]);
            }
        } catch (Exception e) {
            throw new IncorrectLineFormatException("Could not parse line: \n" + line);
        }

        Vector featureVector = new SequentialAccessSparseVector(FEATURES);
        featureVector.assign(featuresDouble);
        return new NamedVector(featureVector, features[LABEL_INDEX]);
    }
}
