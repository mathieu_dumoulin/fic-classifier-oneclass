package com.fujitsu.ca.fic.dataloaders.iris;

import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.SequentialAccessSparseVector;
import org.apache.mahout.math.Vector;

import com.fujitsu.ca.fic.dataloaders.AbstractDatasetFileLoader;
import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;
import com.google.common.collect.HashBiMap;

public class IrisLoader extends AbstractDatasetFileLoader {
    private final static int FEATURES = 4;
    private final static int CATEGORIES = 3;
    private final static int LABEL_INDEX = 4;

    private final static String POSITIVE_LABEL = "Iris-setosa";

    public IrisLoader() {
        categories = HashBiMap.create(CATEGORIES);
        categories.put(0, "Iris-setosa");
        categories.put(1, "Iris-versicolor");
        categories.put(2, "Iris-virginica");
    }

    @Override
    public int getFeaturesCount() {
        return FEATURES;
    }

    @Override
    public int getCategoriesCount() {
        return CATEGORIES;
    }

    @Override
    public String getPositiveLabel() {
        return POSITIVE_LABEL;
    }

    @Override
    public NamedVector parseFields(String line) throws IncorrectLineFormatException {
        double[] featuresDouble = new double[FEATURES];
        String[] features = line.split(",");
        try {
            for (int i = 0; i < FEATURES - 1; i++) {
                featuresDouble[i] = Double.parseDouble(features[i]);
            }
        } catch (Exception e) {
            throw new IncorrectLineFormatException(e.getMessage());
        }

        Vector featureVector = new SequentialAccessSparseVector(FEATURES);
        featureVector.assign(featuresDouble);
        return new NamedVector(featureVector, features[LABEL_INDEX]);
    }
}
