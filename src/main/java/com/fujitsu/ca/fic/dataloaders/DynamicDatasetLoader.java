package com.fujitsu.ca.fic.dataloaders;

import org.apache.mahout.math.NamedVector;

import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;

public interface DynamicDatasetLoader {
    public boolean hasNext();

    public NamedVector getNext() throws IncorrectLineFormatException;

    int getFeaturesCount();

    int getCategoriesCount();

    String getPositiveLabel();
}
