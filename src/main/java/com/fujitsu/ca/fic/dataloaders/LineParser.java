package com.fujitsu.ca.fic.dataloaders;

import org.apache.mahout.math.NamedVector;

import com.fujitsu.ca.fic.exceptions.IncorrectLineFormatException;

public interface LineParser {

    public abstract NamedVector parseFields(String line) throws IncorrectLineFormatException;

}
