package com.fujitsu.ca.fic.mapreduce;

import java.io.IOException;

import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.NullWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Reducer;
import org.apache.mahout.math.NamedVector;
import org.apache.mahout.math.VectorWritable;

import com.fujitsu.ca.fic.classifiers.perceptron.Perceptron;

public class FindConfidentExamplesReducer extends Reducer<LongWritable, VectorWritable, NullWritable, Text> {
    protected Perceptron perceptron;// Protected for testing purposes

    @Override
    protected void reduce(LongWritable key, Iterable<VectorWritable> trainingExamples, Context context) throws IOException,
            InterruptedException {

        for (VectorWritable example : trainingExamples) {
            NamedVector nv = (NamedVector) example.get();

            double score = perceptron.classifyScalar(nv);
            context.write(NullWritable.get(), toText(score, nv));
        }
    }

    private static Text toText(double score, NamedVector nv) {
        StringBuilder sb = new StringBuilder();
        sb.append(score);
        sb.append(",");
        sb.append(nv.getName() + ",");
        for (int i = 0; i < nv.size(); i++) {
            sb.append(String.valueOf(nv.get(i)));
            if (i < nv.size() - 1)
                sb.append(",");
        }
        return new Text(sb.toString());
    }

    @Override
    protected void setup(Context context) throws IOException, InterruptedException {
        super.setup(context);
        perceptron = Perceptron.loadFromFile(context.getConfiguration());
    }

    // For Test Purposes Only!
    protected void setupTest(Context context) throws IOException, InterruptedException {
        super.setup(context);
    }
}
