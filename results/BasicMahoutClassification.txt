l test Accuracy = 89.99
=======================================================
Confusion Matrix
-------------------------------------------------------
a       b       <--Classified as
39595   52       |  39647   a     = 0
4898    4918     |  9816    b     = 1



Classification report:
AUC               : 0.9840
Positive precision: 50.10
Positive recall   : 98.95
Negative precision: 50.10
Negative recall   : 98.95
True negative rate: 88.99
Accuracy          : 89.99